const path = require('path');
const { defineConfig } = require('@vue/cli-service')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = defineConfig({
  transpileDependencies: true,
  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
  productionSourceMap: false,
  configureWebpack: config => {
    //生产环境取消 console.log
    if (process.env.NODE_ENV === 'production') {
      config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
    }
  },
  chainWebpack: (config) => {
    config.resolve.alias
        .set('@$', resolve('src'))
        .set('@api', resolve('src/api'))
        .set('@assets', resolve('src/assets'))
        .set('@comp', resolve('src/components'))
        .set('@views', resolve('src/views'))

    //生产环境，开启js\css压缩
    if (process.env.NODE_ENV === 'production') {
      config.plugin('compressionPlugin').use(new CompressionPlugin({
        test: /\.(js|css|less)$/, // 匹配文件名
        threshold: 10240, // 对超过10k的数据压缩
        deleteOriginalAssets: false // 不删除源文件
      }))
    }

    config.plugin('html').tap(args => {
      args[0].title = '数字化云工厂';
      return args;
    })

  },
  devServer: {
    port: 8080,
    proxy: {
      /*'/dashboard': {
        target: 'http://10.5.54.109:9121',
        ws: false,
        changeOrigin: true
      },*/
      '/api': {
        target: 'http://10.5.54.109:8888',
        ws: false,
        secure: false,
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/api'
        }
      },
      '/dashboard': {
        target: 'http://10.5.54.109:9121',
        ws: false,
        secure: false,
        changeOrigin: true,
        pathRewrite: {
          '^/': '/'
        }
      },

    }
  },
})
