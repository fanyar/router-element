let menuList = [
    {
        path: '/proofingManage',
        name: 'proofingManage',
        meta: {title: '翻样管理',icon: 'HomeFilled'},
        redirect: '/proofingManage/MaterialProofing',
        component: () => import('../views/menu/menu'),
        children: [
            {
                path: '/proofingManage/MaterialProofing',
                name: 'materialProofing',
                meta: {title: '料单翻样', nativeTitle:'翻样管理|料单翻样'},
                component: () => import('../views/proofingManage/MaterialProofing')
            },
            {
                path: '/proofingManage/MaterialApproval',
                name: 'materialApproval',
                meta: {title: '料单审核',nativeTitle:'翻样管理|料单审核'},
                component: () => import('../views/proofingManage/MaterialApproval')
            },
        ]
    },
    {
        path: '/sysConfiguration',
        name: 'sysConfiguration',
        meta: {title: '系统配置',icon: 'HomeFilled'},
        redirect: '/sysConfiguration/projectManagement',
        component: () => import('../views/menu/menu'),
        children: [
            {
                path: '/sysConfiguration/projectManagement',
                name: 'projectManagement',
                meta: {title: '项目管理', nativeTitle:'系统配置|项目管理'},
                component: () => import('../views/sysConfiguration/ProjectManagement')
            },
            {
                path: '/sysConfiguration/EquipmentModel',
                name: 'equipmentModel',
                meta: {title: '设备型号',nativeTitle:'系统配置|设备型号'},
                component: () => import('../views/sysConfiguration/EquipmentModel')
            },
            {
                path: '/sysConfiguration/WeightCalculate',
                name: 'WeightCalculate',
                meta: {title: '重量计算',nativeTitle:'系统配置|重量计算'},
                component: () => import('../views/sysConfiguration/WeightCalculate')
            },
            {
                path: '/sysConfiguration/MaterialListApprove',
                name: 'MaterialListApprove',
                meta: {title: '料单审批',nativeTitle:'系统配置|料单审批'},
                component: () => import('../views/sysConfiguration/MaterialListApprove')
            },
        ]
    },
    {
        path: '/basicConfiguration',
        name: 'basicConfiguration',
        meta: {title: '基础配置',icon: 'HomeFilled'},
        redirect: '/basicConfiguration/ProjectConfig',
        component: () => import('../views/menu/menu'),
        children: [
            {
                path: '/basicConfiguration/ProjectConfig',
                name: 'ProjectConfig',
                meta: {title: '项目配置', nativeTitle:'基础配置|项目配置'},
                component: () => import('../views/basicConfiguration/ProjectConfig')
            },
            {
                path: '/basicConfiguration/ProcessingPlantConfig',
                name: 'ProcessingPlantConfig',
                meta: {title: '加工厂配置',nativeTitle:'基础配置|加工厂配置'},
                component: () => import('../views/basicConfiguration/ProcessingPlantConfig')
            },
            {
                path: '/basicConfiguration/WorkAreaConfig',
                name: 'WorkAreaConfig',
                meta: {title: '工区配置',nativeTitle:'基础配置|工区配置'},
                component: () => import('../views/basicConfiguration/WorkAreaConfig')
            },
        ]
    },
    {
        path: '/proofing/approval',
        name: 'approval',
        meta: {title: '审核',icon: 'IceDrink'},
        component: () => import('../views/proofingManage/MaterialApproval')
    },
    {
        path: '/proofing2',
        name: 'proofing2',
        meta: {title: '翻样管理2',icon: 'HomeFilled'},
        redirect: '/proofing/MaterialProofing',
        component: () => import('../views/menu/menu'),
    },


]

export default menuList;