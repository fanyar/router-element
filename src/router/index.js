import { createRouter, createWebHistor, createWebHashHistory, Router } from 'vue-router'
import menuList from './router';
import store from "@/store"
import { generateChildRouters,arrayToTree } from '@/utils/util'

const routes = [
    // ...menuList,
  {
    path: '/',
    name: 'index',
    redirect: '/main/home'
  },
  {
    path: '/login',
    name: 'login',
    meta: {title: '登录'},
    component: () => import('../views/login/login.vue')
  },
  {
    path: '/404',
    name: '404',
    component: () => import('../views/404.vue')
  },
  {
    path: '/:pathMach(.*)',
    redirect: '/404'
  },
  {
    path: '/main',
    name: 'main',
    meta: {title: 'main',icon: 'Location'},
    redirect: '/main/home',
    component: () => import('../views/menu/menu'),
    children: [
      {
        path: '/main/home',
        name: 'home',
        meta: {title: 'home'},
        component: () => import('../components/demo/HomeView.vue')
      },
      {
        path: '/main/hellowWorld',
        name: 'hellowWorld',
        meta: {title: 'hellowWorld'},
        component: () => import('../components/demo/HelloWorld.vue')
      },
    ]
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

let isF = false  //这个是用于判断动态路由是否已经被获取
router.beforeEach(async (to, from, next) => {
  if (to.path == '/login') {
    next();
  }
  debugger
  if (isF) {
    next();
  } else {
    let res = {data:{}};
    res.data = {"resultCode":200,"resultMessage":"SUCCESS","resultData":
          [
            // {"area_name":"隧道掘进","auth_domain":"comm_menu","cah_id":42482,"crar_id":"211-3","area_code":"204","menu_url":"","area_id":211,"target":"/view/mav?viewParam=","domain_id":"6","role_name":"系统管理员","role_id":3,"order_marks":1,"pt_id":0,"menu_name":"系统管理","level_code":"xe61d","id":6},
            // {"area_name":"隧道掘进","auth_domain":"comm_menu","cah_id":42489,"crar_id":"211-3","area_code":"204","menu_url":"common/commDictWaterfall","area_id":211,"target":"/view/mav?viewParam=","domain_id":"100","role_name":"系统管理员","role_id":3,"order_marks":1,"pt_id":51,"menu_name":"通用字典","level_code":"xe6bf","id":100},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151291,"crar_id":"203-3","area_code":"","menu_url":"/common/commTaskIndex__treeDomain=comm_case","area_id":203,"target":"/view/mav?viewParam=","domain_id":"114","role_name":"系统管理员","role_id":3,"order_marks":1,"pt_id":113,"menu_name":"分部分项","level_code":"xe6bf","id":114},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151295,"crar_id":"203-3","area_code":"","menu_url":"/reinforced/rebarProfileIndex","area_id":203,"target":"/view/mav?viewParam=","domain_id":"148","role_name":"系统管理员","role_id":3,"order_marks":1,"pt_id":147,"menu_name":"重量计量配置","level_code":"xe6bf","id":148},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151292,"crar_id":"203-3","area_code":"","menu_url":"/reinforced/reinforcedLineTaskIndex","area_id":203,"target":"/view/mav?viewParam=","domain_id":"117","role_name":"系统管理员","role_id":3,"order_marks":2,"pt_id":113,"menu_name":"部品任务管理","level_code":"xe6bf","id":117},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151296,"crar_id":"203-3","area_code":"","menu_url":"/reinforced/rebarOptimize","area_id":203,"target":"/view/mav?viewParam=","domain_id":"149","role_name":"系统管理员","role_id":3,"order_marks":2,"pt_id":147,"menu_name":"优化套材计算","level_code":"xe6bf","id":149},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151289,"crar_id":"203-3","area_code":"","menu_url":"/common/commCaseIndex","area_id":203,"target":"/view/mav?viewParam=","domain_id":"112","role_name":"系统管理员","role_id":3,"order_marks":2,"pt_id":51,"menu_name":"项目管理","level_code":"xe6bf","id":112},
            // {"area_name":"隧道掘进","auth_domain":"comm_menu","cah_id":42488,"crar_id":"211-3","area_code":"204","menu_url":"common/commonUnitIndex","area_id":211,"target":"/view/mav?viewParam=","domain_id":"51","role_name":"系统管理员","role_id":3,"order_marks":3,"pt_id":0,"menu_name":"基础数据","level_code":"xe6bf","id":51},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151288,"crar_id":"203-3","area_code":"","menu_url":"/common/commonUnitIndex","area_id":203,"target":"/view/mav?viewParam=","domain_id":"51","role_name":"系统管理员","role_id":3,"order_marks":3,"pt_id":0,"menu_name":"基础数据","level_code":"xe6bf","id":51},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151293,"crar_id":"203-3","area_code":"","menu_url":"/reinforced/reinforcedLineTaskAction","area_id":203,"target":"/view/mav?viewParam=","domain_id":"118","role_name":"系统管理员","role_id":3,"order_marks":3,"pt_id":113,"menu_name":"部品任务执行","level_code":"xe6bf","id":118},
            // {"area_name":"隧道掘进","auth_domain":"comm_menu","cah_id":42484,"crar_id":"211-3","area_code":"204","menu_url":"common/commTree","area_id":211,"target":"/view/mav?viewParam=","domain_id":"37","role_name":"系统管理员","role_id":3,"order_marks":4,"pt_id":6,"menu_name":"文件管理","level_code":"xe6bf","id":37},
            // {"area_name":"隧道掘进","auth_domain":"comm_menu","cah_id":42485,"crar_id":"211-3","area_code":"204","menu_url":"common/commUnitIndex","area_id":211,"target":"/view/mav?viewParam=","domain_id":"52","role_name":"系统管理员","role_id":3,"order_marks":5,"pt_id":6,"menu_name":"单位管理","level_code":"xe6bf","id":52},
            // {"area_name":"隧道掘进","auth_domain":"comm_menu","cah_id":42483,"crar_id":"211-3","area_code":"204","menu_url":"common/commOrganization__orgId=0","area_id":211,"target":"/view/mav?viewParam=","domain_id":"16","role_name":"系统管理员","role_id":3,"order_marks":6,"pt_id":6,"menu_name":"机构人员配置","level_code":"","id":16},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151290,"crar_id":"203-3","area_code":"","menu_url":"","area_id":203,"target":"/view/mav?viewParam=","domain_id":"113","role_name":"系统管理员","role_id":3,"order_marks":9,"pt_id":0,"menu_name":"部品管理","level_code":"xe6bf","id":113},
            {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151298,"crar_id":"203-3","area_code":"","menu_url":"","area_id":203,"target":"/view/mav?viewParam=","domain_id":"220","role_name":"系统管理员","role_id":3,"order_marks":9,"pt_id":219,"menu_name":"系统配置","level_code":"xe6bf","id":220},
            {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151300,"crar_id":"203-3","area_code":"","menu_url":"/sysConfiguration/EquipmentModel","area_id":203,"target":"/view/mav?viewParam=","domain_id":"222","role_name":"系统管理员","role_id":3,"order_marks":9,"pt_id":220,"menu_name":"设备型号","level_code":"xe6bf","id":222},
            // {"area_name":"隧道掘进","auth_domain":"comm_menu","cah_id":42486,"crar_id":"211-3","area_code":"204","menu_url":"common/commUnitTree","area_id":211,"target":"/view/mav?viewParam=","domain_id":"101","role_name":"系统管理员","role_id":3,"order_marks":9,"pt_id":6,"menu_name":"单位人员配置","level_code":"xe6bf","id":101},
            {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151297,"crar_id":"203-3","area_code":"","menu_url":"","area_id":203,"target":"/view/mav?viewParam=","domain_id":"219","role_name":"系统管理员","role_id":3,"order_marks":9,"pt_id":0,"menu_name":"数字化平台","level_code":"xe6bf","id":219},
            {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151299,"crar_id":"203-3","area_code":"","menu_url":"/sysConfiguration/ProjectManagement","area_id":203,"target":"/view/mav?viewParam=","domain_id":"221","role_name":"系统管理员","role_id":3,"order_marks":9,"pt_id":220,"menu_name":"项目管理","level_code":"xe6bf","id":221},
            // {"area_name":"隧道掘进","auth_domain":"comm_menu","cah_id":42487,"crar_id":"211-3","area_code":"204","menu_url":"common/commOrganizationRelUnit","area_id":211,"target":"/view/mav?viewParam=","domain_id":"102","role_name":"系统管理员","role_id":3,"order_marks":9,"pt_id":6,"menu_name":"机构单位配置","level_code":"xe6bf","id":102},
            // {"area_name":"云工厂","auth_domain":"comm_menu","cah_id":151294,"crar_id":"203-3","area_code":"","menu_url":"","area_id":203,"target":"/view/mav?viewParam=","domain_id":"147","role_name":"系统管理员","role_id":3,"order_marks":10,"pt_id":0,"menu_name":"优化下料","level_code":"xe6bf","id":147}
          ]};

    const map = new Map()
    let arr = res.data.resultData.filter((item) => !map.has(item['id'] + '') && map.set(item['id'] + '', 1))
    // console.log(arr)
    let list = arrayToTree(arr);
    console.log(list)
    let addRoute = generateChildRouters(list);
    console.log(addRoute);

    // const res = await getMenuNav();
    // const addRoute = addData(res.data);//获取动态路由
    // 获取当前默认路由
    // const currenRoutes = router.options.routes
    // 将404添加进去
    //现在才添加的原因是：作为一级路由，当刷新，动态路由还未加载，路由就已经做了匹配，找不到就跳到了404
    /*if (currenRoutes[currenRoutes.length - 1].path != '/:catchAll(.*)') {
      currenRoutes.push({ path: "/:pathMatch(.*)", redirect: "/404" });
    }*/
    // 将新生成的路由替换原路由
    addRoute.forEach(item => {
      router.addRoute(item);
    });

    console.log(router.getRoutes(), '查看现有路由')

    // 将新生成的路由保存到vuex中
    store.dispatch('setRoutes', addRoute);
    // 更改控制生成路由次数的值
    isF = true
    // 跳转
    //确保addRoute()时动态添加的路由已经被完全加载上去，不然刷新页面可能会导致空白
    next({ ...to, replace: true });
    // 当然在这一步也可以是去判断一下to.matched是否含有数据，如果没有再触发一次beforeEach
    // if(to.matched.length == 0){
    //     router.push(to.path)
    // }else {next()}

  }

})



export default router
