import { createStore } from 'vuex'
import router from '@/router/index.js'

export default createStore({
  state: {
    currentPath: '6',
    currentMenu: {},
    allRoutes: []
  },
  getters: {
  },
  mutations: {
    changeCurrentPath(state,path){
      state.currentPath = path;
    },
    changeCurrentMenu(state,menu){
      state.currentMenu = menu;
    },
    SET_ROUTES(state, payload) {
      state.allRoutes = payload
    }
  },
  actions: {
    setRoutes({ commit }, data) {
      commit('SET_ROUTES', data);
    },
  },
  modules: {
  }
})
