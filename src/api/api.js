
import http from '@/utils/axios/http.js';

//摄像头ID
const getvideosync = (params)=>http.get("/api/videosync/find/list",params);
//测试接口
const queryPageAtMySql = (params)=>http.post("/api/crow/commUser/queryPageAtMySql",params);



//登录
const userLoginCheck = (params)=>http.post("/api/crow/commUser/userLoginCheck",params);
//获取菜单
const getAuthDomain = (params)=>http.post("/api/crow/commAuth/getAuthDomain",params);




export {
  getvideosync,
  userLoginCheck,
  getAuthDomain,
  queryPageAtMySql
}
